plugins {
    application
    kotlin("jvm") version "1.1.61"
}

application {
    mainClassName = "ch.vanwa.AppKt"
}

buildscript {
    dependencies {
        classpath("com.github.jengelman.gradle.plugins:shadow:2.0.1")
    }
}

apply {
	plugin("com.github.johnrengelman.shadow")
}

dependencies {
    compile("com.xenomachina:kotlin-argparser:2.0.3")
    compile("edu.cmu.sphinx:sphinx4-core:5prealpha-SNAPSHOT")
    compile("edu.cmu.sphinx:sphinx4-data:5prealpha-SNAPSHOT")
    compile("org.jetbrains.kotlin:kotlin-stdlib-jre8")
    compile("com.googlecode.json-simple:json-simple:1.1.1")
}

repositories {
    jcenter()
    maven {
        setUrl("https://oss.sonatype.org/content/repositories/snapshots")
    }
}
