/* Copyright © 2017 Sebastian Hugentobler <sebastian@vanwa.ch>
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package ch.vanwa

import java.io.File
import javax.sound.sampled.AudioInputStream
import javax.sound.sampled.AudioSystem

class Wav(val file: File) {
    fun getLength() : Float {
        val inputStream = AudioSystem.getAudioInputStream(this.file)
        inputStream.use {
            return inputStream.frameLength / inputStream.format.frameRate * 1000
        }
    }
}
