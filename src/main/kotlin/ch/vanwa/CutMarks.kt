/* Copyright © 2017 Sebastian Hugentobler <sebastian@vanwa.ch>
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package ch.vanwa

import java.io.File
import java.io.FileInputStream
import java.io.FileReader
import java.io.InputStream
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Paths
import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import edu.cmu.sphinx.api.Configuration
import edu.cmu.sphinx.api.SpeechResult
import edu.cmu.sphinx.api.StreamSpeechRecognizer

class CutMarks {
    val phraseOffset = 2000

    fun findInFile(input: File, phrase: String, configuration: Configuration) : List<Long> {
        val recognizer = StreamSpeechRecognizer(configuration)
        val stream = FileInputStream(input)

        recognizer.startRecognition(stream)
        var result : SpeechResult? = recognizer.getResult()

        val locations = mutableListOf<Long>()

        while (result != null) {
            val hypothesis = result.getHypothesis()
            if (hypothesis.toLowerCase().contains(phrase)) {
                val time = result.getResult().getCollectTime()
                locations.add(time)
            }
            result = recognizer.getResult()
        }

        recognizer.stopRecognition()

        return locations
    }

    fun getState(stateFile: File) : JSONObject {
        if (!stateFile.exists()) {
            Files.write(Paths.get(stateFile.getPath()), listOf("{}"), Charset.forName("UTF-8"));
        }

        return JSONParser().parse(FileReader(stateFile.getPath())) as JSONObject
    }

    fun saveState(state: JSONObject, stateFile: File) {
        stateFile.bufferedWriter().use { out ->
            out.write(state.toJSONString())
        }
    }

    fun findCutMarks(path: String, phrase: String, configuration: Configuration) : List<Long> {
        val inputFiles = File(path).listFiles().filter { it.getName().endsWith(".wav") }.sorted()
        var lengthTotal = 0.0
        val cutMarks = mutableListOf<Double>()
        cutMarks.add(0.0)

        val stateFile = File(path, ".state")
        val state = getState(stateFile)

        for (inputFile in inputFiles) {
            if (state.contains(inputFile.getName())) {
                val savedCutsJson = state.get(inputFile.getName()) as JSONArray
                var savedCuts = mutableListOf<Double>()

                for (savedCut in savedCutsJson) {
                    savedCuts.add(lengthTotal + savedCut as Double - this.phraseOffset)
                }
                savedCuts.sorted().forEach { cutMarks.add(lengthTotal + it) }
            } else {
                val singleCutMarks = findInFile(inputFile, phrase, configuration)
                singleCutMarks.forEach { cutMarks.add(lengthTotal + it - this.phraseOffset) }

                val cutArray = JSONArray()
                singleCutMarks.forEach { cutArray.add(lengthTotal + it - this.phraseOffset) }
                state.put(inputFile.getName(), cutArray)

                saveState(state, stateFile)
            }

            lengthTotal += Wav(inputFile).getLength()
	    }

        cutMarks.add(lengthTotal)

        return cutMarks.map { it.toLong() }
    }
}
