/* Copyright © 2017 Sebastian Hugentobler <sebastian@vanwa.ch>
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package ch.vanwa

import java.util.concurrent.TimeUnit
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import com.xenomachina.argparser.mainBody
import edu.cmu.sphinx.api.Configuration

class Args(parser: ArgParser) {
    val inputDir by parser.storing("-i", "--input-dir",
                                   help="Directory with input files (default: .)").default(".")

    val phrase by parser.storing("-p", "--phrase",
                               help="Phrase to look for (default: chapter)").default("chapter")
}

fun msToTimeStamp(ms: Long) : String {
    var millis = ms

    val hours = TimeUnit.MILLISECONDS.toHours(millis);
    millis -= TimeUnit.HOURS.toMillis(hours);
    val minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
    millis -= TimeUnit.MINUTES.toMillis(minutes);
    val seconds = TimeUnit.MILLISECONDS.toSeconds(millis);
    millis -= TimeUnit.SECONDS.toMillis(seconds);

    return String.format("%02d:%02d:%02d.%03d",
        hours,
        minutes,
        seconds,
        millis
    )
}

fun main(args: Array<String>) = mainBody() {
    val configuration = Configuration()
    configuration.setAcousticModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us")
    configuration.setDictionaryPath("resource:/edu/cmu/sphinx/models/en-us/cmudict-en-us.dict")
    configuration.setLanguageModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us.lm.bin")

    Args(ArgParser(args)).run {
        val cutMarks = CutMarks().findCutMarks(inputDir, phrase, configuration)
        val cutMarksOutput = cutMarks.map { msToTimeStamp(it.toLong()) }.joinToString(" ")

        println("Cuts: ${cutMarksOutput}")
    }
}
