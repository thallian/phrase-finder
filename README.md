# Phrase Finder
This tries to find phrases in audiofiles.

I wrote this because the chapter marks in my audible audiobooks were useless.

It uses [Sphinx4](https://github.com/cmusphinx/sphinx4) for speech recognition
and it is quite slow because it works only with the CPU but that does not
bother me too much, as it only runs once for every audiobook.

# Usage
## Build from source
You need [gradle](https://gradle.org/).

In the source directory run `gradle shadowJar` and find the fat JAR 
at `build/libs/speech-finder-all.jar`.

## Running
```
usage: [-h] [-i INPUT_DIR] [-p PHRASE]

optional arguments:
  -h, --help              show this help message and exit

  -i INPUT_DIR,           Directory with input files (default: .)
  --input-dir INPUT_DIR

  -p PHRASE,              Phrase to look for (default: chapter)
  --phrase PHRASE
```

The input files need to be in 16 bit mono 16000 Hz wav format.

In order to avoid a crash from memory exhaustion you need to split big files into
multiple smaller ones (you need to play around with sizes to find out what you
need on a given machine and there is nothing that stops you from only putting 
one big file into the input directory).

The program prints the cut marks in milliseconds to stdout (in the format 
HH:mm:ss.SSS), for example: `Cuts: 00:00:00.000 00:00:13.120 00:00:48.339 00:01:06.957`.

A helper script (`scripts/split`) is part of this repository. It takes three positional
arguments:

- 1: The file to split
- 2: The output directory for the splitted files
- 3: The cut marks (if empty it reads them from stdin)

Put it all together and you get something that looks like this:

```
ffmpeg -i /src/The\ Dragon\ Reborn.flac -ar 16000 -ac 1 -segment_time 00:30:00 -f segment  "%03d input.wav" && \ 
    java -jar phrase-finder-all.jar -i /src | \
    sed -n -e 's/^Cuts: //p' | \
    ./split /src/The\ Dragon\ Reborn.flac /src/out`
``` 
